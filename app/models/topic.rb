class Topic < ApplicationRecord
    # manualy set: binds the topic id stored in a vote to a topic and 
    # tells ruby to delete votes that are bound to a topic that is getting deleted
    has_many :votes, dependent: :destroy
end
